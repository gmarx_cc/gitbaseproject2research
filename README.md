# GitBaseProject2Research

This is an example guide repository to develop a research project based on the git control version's philosophy. This example repository could be reproduced for newer repositories as a guidance for related projects.

# A Basic Git Introduction

First of all try to install the git system for your own OS visiting and following some steps [here](https://git-scm.com/downloads).

Git is an  open source version control system to handle from small to large projects with speed and efficiency, with collaborative or private options. Git was designed by **Linus Torvalds** in 2005 for the development of the linux kernel, the system can manage large or small quantity of source code files. But, what is a git repository?

The git flow process is shown in the next figure.

![The three basic stages areas of git process](images/naws3.png)

The process shows three basic stages/areas in the git flow:

1. **The working directory.**
2. **Staging area.**
3. **Repository.**

However, all the stages/areas are together at the same time, just a command will takes your files from one stage to another, let see a basic example.

### working directory
First a **local git repository or local repo** is some kind supervised space in your computer that informs you what files are added, deleted or modified, this space is known as **working directory.** Thus, the git system first ask you which files would you like to **track** any change done in the files. This is done using the "`git add`" command on your OS terminal prompt; a very useful list of commands can be founded in [git cheat sheet](). In the other hand, for large projects there are some GUI (Graphic User Interface) that can be used like [smart git](https://www.syntevo.com/smartgit/) or [source tree](https://www.sourcetreeapp.com); my personal recommendation is smartgit.

This time, as an example smart git screen shots are going to be used, then, if you install a GUI system you will see your **working directory** like this:

![Working Directory. 1)At left: the repository name, 2)at right: the files.](images/workingDirectory.jpeg)

then, you can see the file's status. In the example you are seeing **untracked** and **modified** files. **Untracked** means that there are new files and they are not supervised by the git system. While the **modified** ones, are files already supervised by git, and have been changed. Also you will find other status options like **rename** and **deleted**, meaning of course that those files have been renamed or deleted at the **working directory**.

If you are using a command based git system, you can check the status of your working directory with the `git status` command:

```console
Dir marx$ git status
```
here we are considering that we are at the *Dir* directory. The answer of the system will be something like this:

```console
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   README.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	
	introduction.txt

no changes added to commit (use "git add" and/or "git commit -a")
```

as you can see, the system informs you that there are modified and untracked files. 

After this you are ready to add the new files to the **staging area**.

### staging area

To **stage** a file or group of files is just the preparation to finally make a commit. Suppose you are working on two options, one is ready, and the other needs more work. Then you can stage the part that is **ok** and commit it. Thus, you need to add the file with the stage button int the GUI based git:

![Stage button.](images/stage.png)

You have to repeat this for every file you want to add, or you can select several files at the same time. 

In the command based version, you can add an specific file with the `git add` command:

```console
Dir marx$ git add Readme.md
```
if you need to add all changes of your working directory it is possible by using:
```
Dir marx$ git add *
```

### Commit ###

To be able to finish the commit process must be sure to setting up a name and e-mail address, then if you've never used git before, first you need to set up both. Run the following commands to let git know your name and e-mail address. If you already did it, skip this step.

```console
git config --global user.name "Your Name"
git config --global user.email "your_email@whatever.com"
```
after that you also need to be sure of make a simple change on any of the tracking files like the `Readme.md` file, afterwards you can use the 'git commit' command:

```console
git commit -m "Changes for Readme file"
```
If you are following the GUI method, you will see a button like this:

![The commit button](images/commit1.png)

after pressing the button you can just add the message that will be related with that version's content, as can be seen on the image you can select what files are going to be commited:
![The message to be commited](images/commit2.png)

Then, when the commit button has been pressed a new dot will appear in the version line time, in this case after the initial commit:

![The ](images/commit3.png)

# There is more #

This is just a basic introduction to control the content of a repository, this repositories can be local or also can be remote. Then terms like stage, working copy, fetch, pull, push, branch, merge, stash, tag, and remote repository will be common on the git language. However, this is a simple introduction to the git system, if you want to learn a detailed information and commands visit the [git web-page](https://git-scm.com) to find e-books, examples and useful information.


#### Next Ideas to be developed here ####
- Branches
- Tags
- stash 
